import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';
import CreateCard from './CreateCard';
import { MapExistingCards } from './CreateCard';
import Card from './card.json'
import { Provider } from 'react-redux';
import { createStore, combineReducers} from 'redux';
import auth from '../reducers/auth';

let store;

const reducer = combineReducers({
    auth: auth
})

afterEach(()=>{
    cleanup()
})

beforeEach(() => {
    store = createTestStore();
})

const createTestStore = () =>{
    const st = createStore(
        reducer
    ) 
    return st;
}

test('should render card creation window', () => {
    
    render(
        <Provider store={store}>
            <CreateCard offlineComponent={true}></CreateCard>
        </Provider>
    )
    const cardCreationWindow = screen.getByTestId('cardCreationWindow');
    expect(cardCreationWindow).toBeInTheDocument();
})


test('should render a card', () => {
    let card = {data: Card, id: "0"};
    let cardData = [card];
    render(<MapExistingCards cardData={cardData}></MapExistingCards>)
    const cardElement = screen.getByTestId('card-0');
    expect(cardElement).toBeInTheDocument();
});

test('should render three cards', () => {
    let cards = [{data: Card, id: "0"}, {data: Card, id: "1"}, {data: Card, id: "2"}]
    render(<MapExistingCards cardData={cards}></MapExistingCards>)
    let cardElement = screen.getByTestId('card-0');
    expect(cardElement).toBeInTheDocument();
    let cardTwo = screen.getByTestId('card-1');
    expect(cardTwo).toBeInTheDocument();
    let cardThree = screen.getByTestId('card-2');
    expect(cardThree).toBeInTheDocument();
});

test('renders a card with a theme, term and an explanation', () => {
    let card = {data: Card, id: "0"};
    card.data.theme = "sports"
    card.data.term = "bowling"
    card.data.explanation = "throw a ball to pins"
    let cardData = [card];
    render(<MapExistingCards cardData={cardData}></MapExistingCards>)
    const cardElement = screen.getByTestId('card-0');
    expect(cardElement).toBeInTheDocument();
    expect(cardElement).toHaveTextContent("sports")
    expect(cardElement).toHaveTextContent("bowling")
    expect(cardElement).toHaveTextContent("throw a ball to pins")
});


