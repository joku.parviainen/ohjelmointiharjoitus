import React, { useEffect, useState } from "react";
import {db} from '../firebase/firebase';
import { connect } from "react-redux";
import CardBlueprint from './card.json' 

// TODO: Login functionality not in use, replace cards/users/default with cards/users/{userID} when possible

export function CreateCard (props) {

  const [cardData, setCardData]=useState([])
  const [explanation, setExplanation]=useState()
  const [theme, setTheme]=useState()
  const [term, setTerm]=useState()

  const getCardData = async()=>{
    setCardData([])

    const data = await db.collection('cards/users/default/').get();

    if(!data) return;
    data.docs.forEach(card=>{
      setCardData(cardData => [...cardData, {data: card.data(), id: card.id}])
    })
  }

  const removeCardFromDatabase  = async(id) =>{
    db.doc(`cards/users/default/${id}`).delete().then(getCardData);
  }

  const saveCardToDatabase = async() =>{
    if(!theme || !term || !explanation)
    {
      alert('syötä puuttuvat tiedot')
      return;
    } 

    let newCard = CardBlueprint;
    newCard.term = term;
    newCard.theme = theme; 
    newCard.explanation = explanation;
    db.collection('cards/users/default/').add(newCard).then(getCardData);
  } 
 
  useEffect(() => {
    if(!props.offlineComponent)
    {
      getCardData();
    }
  }, [])

  

  return (
    <div style={{height: "100vh", backgroundColor: "#ffff", color: '#b8c6de'}} data-testid={'cardCreationWindow'}>
      <div className='cardCreationWrapper'>
        <h3>Lisää kortti tietokantaan</h3>
        <select type="select" defaultValue="" onChange={e => setTheme(e.target.value)}>
          <option value=""  disabled hidden>Valitse teema</option>
          <option value="Kemia">Kemia</option>
          <option value="Biologia">Biologia</option>
        </select>
        <input type="text" placeholder="Termi" onInput={e => setTerm(e.target.value)}></input>
        <textarea type="text" placeholder="Selitys" onInput={e => setExplanation(e.target.value)}></textarea>
        <button type="button" onClick={() => saveCardToDatabase()} >Lisää kortti</button>
      </div>
      <MapExistingCards cardData={cardData} removeCard={removeCardFromDatabase}/>
    </div>
  );
}


export function MapExistingCards(props)
{
  return (
    <div className="existingCardContainer" >
      {
        props.cardData && props.cardData.map(card=>{
          return(
            <div className="cardContainer" data-testid={`card-${card.id}`} key={card.id}>
              <button style={{float:'right'}} onClick={() => {props.removeCard(card.id)}}>Poista kortti</button>
              <div>
                <h3>Teema: {card.data.theme}</h3>
                <h2>Termi: {card.data.term}</h2>
                <p>selitys: {card.data.explanation}</p>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}



function mapStateToProps(state) {
  return {
    isLoggingOut: state.auth.isLoggingOut,
    logoutError: state.auth.logoutError,
  };
}




export default connect(mapStateToProps)(CreateCard);
