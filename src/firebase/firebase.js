import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyC1P6xATDV8GARJwX_5abaynG8ZZMVNnQw",
    authDomain: "ohjelmointiharjoitus-ad9cc.firebaseapp.com",
    projectId: "ohjelmointiharjoitus-ad9cc",
    storageBucket: "ohjelmointiharjoitus-ad9cc.appspot.com",
    messagingSenderId: "446912312275",
    appId: "1:446912312275:web:a6db7f6197647eb81cf526"
};

export const myFirebase = firebase.initializeApp(firebaseConfig);
export const db = myFirebase.firestore();
//export const db = () => {}
